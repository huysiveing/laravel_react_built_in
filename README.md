# laravel_react_builtin


## Getting started

Please create ```.env``` file in the root of project and replace your enviroment
After you have configured your enviroment please run this:
```
php artisan migrate
```
To migrate the table into your local database


Before run please make sure you have installed the package and vendor 
```
npm install
composer install
```

After you run that, please use this to run your project
```
php artisan serve
npm run dev
```

