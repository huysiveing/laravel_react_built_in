<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION
    ]);
})->name('welcome');

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'verified']], function(){

    Route::get('/', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
 
    Route::controller(CategoryController::class)->group(function () {
        Route::get('/admin/category', 'index')->name('category.index');
        Route::post('/admin/category', 'create')->name('category.create');
        Route::get('/admin/category/{id}', 'edit')->name('category.edit');
        Route::put('/admin/category/{id}', 'update')->name('category.update');
        Route::post('/admin/category/destory/{id}', 'destroy')->name('category.destroy');
    });

});

require __DIR__.'/auth.php';
