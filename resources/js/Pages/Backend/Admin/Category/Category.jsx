import React, { useEffect, useRef, useState } from 'react';

import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, Link } from '@inertiajs/inertia-react';
import CategoryForm from './CategoryForm';
import Pagination from '@/Components/Pagination';
import NavLink from '@/Components/NavLink';
import ModalConfirmation from '@/Components/ModalConfirmation';
import { Inertia } from '@inertiajs/inertia';

export default function Category(props) {
    const [categories, SetCategories] = useState([]);

    const [open, setOpen] = useState(false)
    const cancelButtonRef = useRef(null)

    const [idCate, setCateId] = useState(null)

    useEffect(() => {
        SetCategories(props.category.data);
    }, [props.category])

    const RemoveCategoryItem = (e) => {
        e.preventDefault();

        Inertia.post(route('category.destroy', { id: idCate }), {}, {
            onSuccess: (res) => {
                if (res) {
                    setOpen(false);
                    setCateId(null)
                }
            },
            onError: (err) => {
                setOpen(false);
            }
        })
    }

    return (
        <AuthenticatedLayout
            auth={props.auth}
            errors={props.errors}
        >
            <Head title="Category" />

            {open && <ModalConfirmation open={open} cancelButtonRef={cancelButtonRef} setOpen={setOpen} actionRemove={RemoveCategoryItem} />}

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <CategoryForm props={props} />

                    <div className="flex flex-col pt-10">
                        <div className="overflow-x-auto">
                            <div className=" w-full inline-block align-middle">
                                <div className="overflow-hidden border rounded-lg">
                                    <table className="min-w-full divide-y divide-gray-200">
                                        <thead className="bg-gray-200">
                                            <tr>
                                                <th scope="col" className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase">No</th>
                                                <th scope="col" className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase"> Name</th>
                                                <th scope="col" className="px-6 py-3 text-xs font-bold text-right text-gray-500 uppercase"> Edit</th>
                                                <th scope="col" className="px-6 py-3 text-xs font-bold text-right text-gray-500 uppercase"> Delete </th>
                                            </tr>
                                        </thead>

                                        <tbody className="bg-gray-50 divide-y divide-gray-200">
                                            {categories.map((category, index) => (
                                                <tr key={category.id}>
                                                    <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                                                        {index + 1}
                                                    </td>
                                                    <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                                        {category.name}
                                                    </td>
                                                    <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                                                        <Link className="text-green-500 hover:text-green-700" href={route('category.edit', { id: category.id })}>
                                                            Edit
                                                        </Link>
                                                    </td>
                                                    <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                                                        <a className="text-red-500 hover:text-red-700" href="#" onClick={(e) => {
                                                            setOpen(true);
                                                            setCateId(category.id);
                                                        }}> Delete </a>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>

                                <Pagination links={props.category.links} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
