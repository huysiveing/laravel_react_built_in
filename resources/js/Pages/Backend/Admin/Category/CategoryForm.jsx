


import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Inertia } from '@inertiajs/inertia';
import { useForm } from '@inertiajs/inertia-react';
import { isEmpty } from 'lodash';
import React, { useState } from 'react';

export default function CategoryForm({ props }) {

    const objectSchema = { name: !isEmpty(props.dataCate) ? props.dataCate.name : '' };

    const { data, setData, post, processing, errors, reset, setError } = useForm(objectSchema);

    const [isSaved, SetIsSaved] = useState(false);

    const onHandleChange = (event) => {
        setData(event.target.name, event.target.value);
    };

    const submit = (e) => {
        e.preventDefault();

        if (!isEmpty(props.dataCate)) {
            Inertia.put(route('category.update', { id: props.dataCate.id }), data, {
                onSuccess: (res) => {
                    if (res) {
                        reset();
                        SetIsSaved(true);
                        setData('name', '')
                    }
                },
                onError: (err) => {
                    setError(err);
                }
            })
        } else {
            Inertia.post(route('category.create'), data, {
                onSuccess: (res) => {
                    if (res) {
                        reset();
                        SetIsSaved(true);
                        setData('name', '')
                    }
                },
                onError: (err) => {
                    setError(err);
                }
            })
        }
    };

    if (isSaved) {
        setInterval(() => {
            SetIsSaved(false);
        }, 3000);
    }

    return (
        <div className="max-w-7xl mx-auto">
            <div className="md:grid md:grid-cols-1 md:gap-6">
                <div className="mt-5 md:col-span-2 md:mt-0">
                    <form onSubmit={submit}>
                        <div className="overflow-hidden shadow sm:rounded-md">
                            <div className="bg-white px-4 py-5 sm:p-6">
                                <div className="grid grid-cols-6 gap-6">
                                    <div className="col-span-6 sm:col-span-3">
                                        <InputLabel forInput="name" value="Category Name" />

                                        <TextInput
                                            type="text"
                                            name="name"
                                            value={data.name}
                                            className="mt-1 block w-full"
                                            isFocused={true}
                                            handleChange={onHandleChange}
                                        />

                                        <InputError message={errors.name} className="mt-2" />
                                    </div>
                                </div>
                            </div>
                            <div className="bg-gray-50 px-4 py-3 text-right sm:px-6">

                                {isSaved && (
                                    'Saved.'
                                )}

                                <PrimaryButton className="ml-4" processing={processing}>
                                    Save
                                </PrimaryButton>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}
