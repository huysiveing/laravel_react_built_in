import React from 'react';
import { MegaphoneIcon, XMarkIcon } from '@heroicons/react/20/solid'

export default function BannerPromotion({ isShow = true, bgcolor = 'bg-indigo-600', msg, isRemove = true }) {
    return (
        <>
            {isShow && (
                <div className={bgcolor}>
                    <div className="mx-auto max-w-7xl py-3 px-3 sm:px-6 lg:px-8">
                        <div className="flex flex-wrap items-center justify-between">
                            <div className="flex w-0 flex-1 items-center">
                                <span className="flex rounded-lg bg-red-600 p-2">
                                    <MegaphoneIcon className="h-6 w-6 text-white" aria-hidden="true" />
                                </span>
                                <p className="ml-3 truncate font-medium text-white">
                                    <span className="md:hidden">We announced a new product!</span>
                                    <span className="hidden md:inline">{msg}</span>
                                </p>
                            </div>
                            <div className="order-3 mt-2 w-full flex-shrink-0 sm:order-2 sm:mt-0 sm:w-auto">
                                <a
                                    href="#"
                                    className="flex items-center justify-center rounded-md border border-transparent bg-white px-4 py-2 text-sm font-medium text-red-600 shadow-sm hover:bg-red-50"
                                >
                                    Learn more
                                </a>
                            </div>

                            {isRemove && (
                                <div className="order-2 flex-shrink-0 sm:order-3 sm:ml-3">
                                    <button
                                        type="button"
                                        className="-mr-1 flex rounded-md p-2 hover:bg-red-500 focus:outline-none focus:ring-2 focus:ring-white sm:-mr-2"
                                    >
                                        <span className="sr-only">Dismiss</span>
                                        <XMarkIcon className="h-6 w-6 text-white" aria-hidden="true" />
                                    </button>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            )}
        </>
    );
}
